from rest_framework_mongoengine import serializers
from apps.survey_detail.models import DailySurveyReport, UserSurveysCount


class DailySurveyReportSerializer(serializers.DocumentSerializer):
    class Meta:
        model = DailySurveyReport
        fields = ['diagnosed_as_corona_infected',
                  'diagnosed_as_corona_infected_when',
                  'diagnosed_as_corona_is_recovered',
                  'diagnosed_as_corona_is_symptoms',
                  'is_under_quarantine',
                  'is_under_quarantine_start',
                  'is_under_quarantine_reason',
                  'suffering_status_since_2_weeks_fever',
                  'suffering_status_since_2_weeks_cough',
                  'suffering_status_since_2_weeks_tiredness',
                  'suffering_status_since_6_weeks_breathing_difficulties',
                  'suffering_status_since_6_weeks_sore_throats',
                  'suffering_status_since_6_weeks_aim_in_the_mussels',
                  'temperature_units',
                  'temperature']


class GetUserSurveysCountSerializer(serializers.DocumentSerializer):
    class Meta:
        model = UserSurveysCount
        fields = '__all__'