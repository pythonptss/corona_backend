from rest_framework_mongoengine import routers as merouters

merouter = merouters.DefaultRouter()

from django.urls import include, path, re_path
from . import views

urlpatterns = [
    re_path(r'^v1/survey_detail/(?P<email>[0-9a-z_@.]+)$',
        views.GetDeleteUpdateSurveyDetail.as_view(),
        name='get_delete_update_survey_detail'
        ),
    path('v1/survey_detail/',
        views.GetPostSurveyDetail.as_view(),
        name='get_survey_detail'
        ),
    re_path(r'^v1/user_surveys_count/(?P<email>[0-9a-z_@.]+)$',
        views.GetUserSurveysCount.as_view(),
        name='get_user_surveyS_count'
        ),
    re_path(r'^v1/user_today_survey_detail/(?P<email>[0-9a-z_@.]+)$',
            views.GetTodayUserSurveyDetail.as_view(),
            name='get_user_today_survey_detail'
        ),
]

urlpatterns += merouter.urls