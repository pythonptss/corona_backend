from apps.survey_detail.models import DailySurveyReport
from apps.user_detail.models import UserCharacteristics
from .pagination import CustomPagination
from .serializers import DailySurveyReportSerializer, GetUserSurveysCountSerializer
from rest_framework import status
from rest_framework.response import Response
from rest_framework.generics import ListCreateAPIView
from rest_framework.permissions import IsAuthenticated
from django.conf import settings
from bson import ObjectId
from datetime import datetime
import re


# get(single)/delete/update user detail
class GetDeleteUpdateSurveyDetail(ListCreateAPIView):
    serializer_class = DailySurveyReportSerializer
    permission_classes = (IsAuthenticated,)
    pagination_class = CustomPagination

    def get_query(self, object_id):
        daily_user_surveys = DailySurveyReport.objects(user_characteristics_id=ObjectId(str(object_id))).order_by('-created')
        return daily_user_surveys

    def     get_user_characteristics_object_id_by_email(self, email):
        try:
            user_characteristics = UserCharacteristics.objects(email=str(email)).get()
            user_characteristics_obj_id = user_characteristics.id
        except:
            user_characteristics_obj_id = None
        return user_characteristics_obj_id

    # Get all feed
    def get(self, request, *args, **kwargs):
        email = kwargs.get('email', None)
        user_characteristics_obj_id = self.get_user_characteristics_object_id_by_email(email=email)
        if user_characteristics_obj_id:
            daily_user_surveys = self.get_query(str(user_characteristics_obj_id))
            paginate_queryset = self.paginate_queryset(daily_user_surveys)
            serializer = self.serializer_class(paginate_queryset, many=True)
            return self.get_paginated_response(serializer.data)
        else:
            return Response({"message": "No User Detail Found Respective to the email."},
                            status=status.HTTP_404_NOT_FOUND)


# get(all)/post archive feed.
class GetPostSurveyDetail(ListCreateAPIView):

    permission_classes = (IsAuthenticated,)
    pagination_class = CustomPagination

    def get_queryset(self, user_characteristics_id):
        survey_detail = DailySurveyReport.objects(user_characteristics_id = ObjectId(str(user_characteristics_id))).order_by('-created')
        return survey_detail

    def get_user_detail_by_email(self, email):
        try:
            user_characteristics = UserCharacteristics.objects(email=str(email)).get()
        except:
            user_characteristics = None
        return user_characteristics

    def is_same_date_document_exists(self, user_characteristics_id):

        now = datetime.now()
        date_str = now.strftime('%m/%d/%Y')
        datetime_object = datetime.strptime(date_str, '%m/%d/%Y')
        doc_ = datetime.combine(
            datetime_object,
            datetime.min.time()
        )
        db = settings.PYMONGO_CONN
        client = db[str(settings.MONGO_SERVER['dbname'])]
        db_data = client['daily_survey_report']
        cursor = db_data.find_one(
            {'created': {'$eq': doc_},
             'user_characteristics_id': ObjectId(str(user_characteristics_id))
            })
        db.close()

        if cursor:
            return True
        else:
            return False

    RE_D = re.compile('\d')

    def has_digits(self, string):
        res = self.RE_D.search(string)
        return res is not None

    def extract_nbr(self, input_str):

        if self.has_digits(str(input_str)):
            if input_str is None or input_str == '':
                return 0
            out_number = ''
            for ele in input_str:
                if ele.isdigit():
                    out_number += ele
            return int(out_number)
        else:
            return 0

    def make_document(self, request):

        document = {}
        is_valid = False
        """
        Receiving data:
        -   user_characteristics_id
        -   diagnosed_as_corona_infected
        -   diagnosed_as_corona_infected_when
        -   diagnosed_as_corona_is_recovered
        -   diagnosed_as_corona_is_symptoms
        -   is_under_quarantine
        -   is_under_quarantine_start
        -   is_under_quarantine_reason
        -   suffering_status_since_2_weeks_fever
        -   suffering_status_since_2_weeks_cough
        -   suffering_status_since_2_weeks_tiredness
        -   suffering_status_since_6_weeks_breathing_difficulties
        -   suffering_status_since_6_weeks_sore_throats
        -   suffering_status_since_6_weeks_aim_in_the_mussels
        -   email
        -   created
        -   temprature
        -   temprature_units   
        """

        email = request.POST.get('email')
        user_characteristics_id_string = None
        if email:
            user_detail_object = self.get_user_detail_by_email(email)
            if user_detail_object:
                document['user_characteristics_id'] = user_detail_object.id
                user_characteristics_id_string = str(user_detail_object.id)
                is_valid = True

        diagnosed_as_corona_infected = request.POST.get('diagnosed_as_corona_infected')
        if diagnosed_as_corona_infected:
            document['diagnosed_as_corona_infected'] = self.extract_nbr(diagnosed_as_corona_infected)

        # date time needs to check
        diagnosed_as_corona_infected_when = request.POST.get('diagnosed_as_corona_infected_when')
        if diagnosed_as_corona_infected_when:
            datetime_object = datetime.strptime(diagnosed_as_corona_infected_when, '%m/%d/%Y')
            document['diagnosed_as_corona_infected_when'] = datetime.combine(datetime_object, datetime.min.time())

        diagnosed_as_corona_is_recovered = request.POST.get('diagnosed_as_corona_is_recovered')
        if diagnosed_as_corona_is_recovered:
            document['diagnosed_as_corona_is_recovered'] = self.extract_nbr(diagnosed_as_corona_is_recovered)

        diagnosed_as_corona_is_symptoms = request.POST.get('diagnosed_as_corona_is_symptoms')
        if diagnosed_as_corona_is_symptoms:
            document['diagnosed_as_corona_is_symptoms'] = str(diagnosed_as_corona_is_symptoms)

        # question (7)
        is_under_quarantine = request.POST.get('is_under_quarantine')
        if is_under_quarantine:
            document['is_under_quarantine'] = self.extract_nbr(is_under_quarantine)

        is_under_quarantine_start = request.POST.get('is_under_quarantine_start')
        if is_under_quarantine_start:
            datetime_object = datetime.strptime(is_under_quarantine_start, '%m/%d/%Y')
            document['is_under_quarantine_start'] = datetime.combine(datetime_object, datetime.min.time())

        is_under_quarantine_reason = request.POST.get('is_under_quarantine_reason')
        if is_under_quarantine_reason:
            document['is_under_quarantine_reason'] = str(is_under_quarantine_reason)

        suffering_status_since_2_weeks_fever = request.POST.get('suffering_status_since_2_weeks_fever')
        if suffering_status_since_2_weeks_fever:
            document['suffering_status_since_2_weeks_fever'] = suffering_status_since_2_weeks_fever

        suffering_status_since_2_weeks_cough = request.POST.get('suffering_status_since_2_weeks_cough')
        if suffering_status_since_2_weeks_cough:
            document['suffering_status_since_2_weeks_cough'] = suffering_status_since_2_weeks_cough

        suffering_status_since_2_weeks_tiredness = request.POST.get('suffering_status_since_2_weeks_tiredness')
        if suffering_status_since_2_weeks_tiredness:
            document['suffering_status_since_2_weeks_tiredness'] = suffering_status_since_2_weeks_tiredness

        suffering_status_since_6_weeks_breathing_difficulties = request.POST.get('suffering_status_since_6_weeks_breathing_difficulties')
        if suffering_status_since_6_weeks_breathing_difficulties:
            document['suffering_status_since_6_weeks_breathing_difficulties'] = suffering_status_since_6_weeks_breathing_difficulties

        suffering_status_since_6_weeks_sore_throats = request.POST.get('suffering_status_since_6_weeks_sore_throats')
        if suffering_status_since_6_weeks_sore_throats:
            document['suffering_status_since_6_weeks_sore_throats'] = suffering_status_since_6_weeks_sore_throats

        suffering_status_since_6_weeks_aim_in_the_mussels = request.POST.get('suffering_status_since_6_weeks_aim_in_the_mussels')
        if suffering_status_since_6_weeks_aim_in_the_mussels:
            document['suffering_status_since_6_weeks_aim_in_the_mussels'] = suffering_status_since_6_weeks_aim_in_the_mussels

        temperature = request.POST.get('temperature')
        if temperature:
            document['temperature'] = str(temperature)

        temperature_units = request.POST.get('temperature_units')
        if temperature_units:
            document['temperature_units'] = str(temperature_units)


        # set created
        now = datetime.now()
        date_str = now.strftime('%m/%d/%Y')
        date_object = datetime.strptime(date_str, '%m/%d/%Y')
        created = datetime.combine(date_object, datetime.min.time())
        document['created'] = created

        return {
            "document": document,
            "email": email,
            "user_characteristics_id_string": user_characteristics_id_string,
            "is_valid": is_valid
        }

    # Create a new user characteristics
    def post(self, request, *args, **kwargs):
        daily_report_document = self.make_document(request)

        # If same date data exists overwrite that.
        is_valid = daily_report_document["is_valid"]

        if is_valid:

            is_document_exists = self.is_same_date_document_exists(daily_report_document["user_characteristics_id_string"])

            if is_document_exists:
                params = {}
                params['$set'] = daily_report_document["document"]
                db = settings.PYMONGO_CONN
                client = db[str(settings.MONGO_SERVER['dbname'])]
                db_data = client['daily_survey_report']

                now = datetime.now()
                date_str = now.strftime('%m/%d/%Y')
                date_object = datetime.strptime(date_str, '%m/%d/%Y')
                created = datetime.combine(date_object, datetime.min.time())

                db_data.update_one({
                    'created': {'$eq': created}, 'user_characteristics_id': ObjectId(daily_report_document["user_characteristics_id_string"])
                },
                    params, upsert=False
                )
                db.close()
                return Response({"message": "Daily Survey Updated Successfully."}, status = status.HTTP_205_RESET_CONTENT)

            else:
                # insert
                params_to_save = daily_report_document["document"]

                # make entry in archive data
                db = settings.PYMONGO_CONN
                client = db[str(settings.MONGO_SERVER['dbname'])]
                db_data = client['daily_survey_report']
                db_data.save(params_to_save)
                db.close()
                return Response({"message": "Daily Survey Created Successfully"}, status = status.HTTP_201_CREATED)
        else:
            return Response({"message": "No User Detail Found Respective to the email."}, status=status.HTTP_404_NOT_FOUND)


class GetUserSurveysCount(ListCreateAPIView):
    
    permission_classes = (IsAuthenticated,)

    def get_query(self, object_id):
        user_surveys_count = DailySurveyReport.objects(user_characteristics_id=ObjectId(str(object_id))).count()
        return user_surveys_count

    def get_user_characteristics_object_id_by_email(self, email):
        try:
            user_characteristics = UserCharacteristics.objects(email=str(email)).get()
            user_characteristics_obj_id = user_characteristics.id
        except:
            user_characteristics_obj_id = None
        return user_characteristics_obj_id

    def get(self, request, *args, **kwargs):
        user_surveys_count = 0
        serializer_class = GetUserSurveysCountSerializer
        email = kwargs.get('email', None)

        user_characteristics_obj_id = self.get_user_characteristics_object_id_by_email(email=email)
        if user_characteristics_obj_id:
            user_surveys_count = self.get_query(str(user_characteristics_obj_id))

        data = {"user_surveys_count": user_surveys_count}
        results = serializer_class(data).data
        return Response(results)


class GetTodayUserSurveyDetail(ListCreateAPIView):
    permission_classes = (IsAuthenticated,)

    def get_today_user_survey_document(self, user_characteristics_id):

        now = datetime.now()
        date_str = now.strftime('%m/%d/%Y')
        datetime_object = datetime.strptime(date_str, '%m/%d/%Y')
        doc_ = datetime.combine(
            datetime_object,
            datetime.min.time()
        )
        db = settings.PYMONGO_CONN
        client = db[str(settings.MONGO_SERVER['dbname'])]
        db_data = client['daily_survey_report']
        cursor = db_data.find_one(
            {'created': {'$eq': doc_},
             'user_characteristics_id': ObjectId(str(user_characteristics_id))
            })
        db.close()

        if cursor:
            return cursor
        else:
            return None

    def get_user_characteristics_object_id_by_email(self, email):
        try:
            user_characteristics = UserCharacteristics.objects(email=str(email)).get()
            user_characteristics_obj_id = user_characteristics.id
        except:
            user_characteristics_obj_id = None
        return user_characteristics_obj_id

    def get(self, request, *args, **kwargs):
        user_surveys_count = 0
        serializer_class = DailySurveyReportSerializer
        email = kwargs.get('email', None)
        user_characteristics_obj_id = self.get_user_characteristics_object_id_by_email(email=email)
        if user_characteristics_obj_id:
            today_user_survey_document = self.get_today_user_survey_document(str(user_characteristics_obj_id))
            if today_user_survey_document:
                results = serializer_class(today_user_survey_document).data
                return Response(results)

        results = serializer_class().data
        return Response(results)