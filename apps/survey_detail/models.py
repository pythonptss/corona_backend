from mongoengine import Document, fields


class DailySurveyReport(Document):
	user_characteristics_id = fields.ObjectIdField(required=True)
	diagnosed_as_corona_infected = fields.IntField(default=2)
	diagnosed_as_corona_infected_when = fields.DateTimeField()
	diagnosed_as_corona_is_recovered = fields.IntField(default=2)
	diagnosed_as_corona_is_symptoms = fields.StringField(default="")
	is_under_quarantine = fields.IntField(default=2)
	is_under_quarantine_start = fields.DateTimeField()
	is_under_quarantine_reason = fields.StringField(default="")
	suffering_status_since_2_weeks_fever = fields.BooleanField(default=False)
	suffering_status_since_2_weeks_cough = fields.BooleanField(default=False)
	suffering_status_since_2_weeks_tiredness = fields.BooleanField(default=False)
	suffering_status_since_6_weeks_breathing_difficulties = fields.BooleanField(default=False)
	suffering_status_since_6_weeks_sore_throats = fields.BooleanField(default=False)
	suffering_status_since_6_weeks_aim_in_the_mussels = fields.BooleanField(default=False)
	created = fields.DateTimeField()
	email = fields.StringField(default="")
	temperature_units = fields.StringField(default="fahrenheit")
	temperature = fields.StringField(default="")


class UserSurveysCount(Document):
	user_surveys_count = fields.ObjectIdField(required=True)