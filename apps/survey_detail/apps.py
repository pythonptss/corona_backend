from django.apps import AppConfig


class SurveyDetailConfig(AppConfig):
    name = 'survey_detail'
