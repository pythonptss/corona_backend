from django.urls import path
from django.conf.urls import url

# importing views from views..py
from apps.garmin_api.views import callback_request_token

urlpatterns = [
    url(r'^(?P<token>[0-9A-Za-z_-]+)/(?P<email>[0-9A-Za-z_.%]+)/(?P<lang>[0-9A-Za-z]+)/callback/$', callback_request_token, name="callback request token")
]