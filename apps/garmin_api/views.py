from django.shortcuts import render
import subprocess
from django.conf import settings
import requests
import arrow
from apps.garmin_api.api.data_utils import GarminApiDataUtils
from django.shortcuts import redirect


def get_inc_utc_time_stamp(minutes):
    utc = arrow.utcnow()
    utc = utc.shift(minutes=+minutes)
    return utc.timestamp


def __fetch_request_data(qstring):
    q_string_obj = {"oauth_token": "", "oauth_token_secret": ""}
    try:
        q_string_obj = dict((itm.split('=')[0], itm.split('=')[1]) for itm in qstring.split('&'))
    except Exception as e:
        pass
    if "oauth_token_secret" not in q_string_obj:
        q_string_obj = {"oauth_token": "", "oauth_token_secret": ""}
    if "oauth_token" not in q_string_obj:
        q_string_obj = {"oauth_token": "", "oauth_token_secret": ""}

    return q_string_obj


def callback_request_token(request, token, email, lang):
    email = email.replace("uyhjhjkjk989asdfasdhhjjjjnhjhjjhjhj", "@")
    oauth_verifier = request.GET.get("oauth_verifier", None)
    oauth_token = request.GET.get("oauth_token", None)
    oauth_token_secret = token
    oauth_consumer_key = "9e5a555d-42fd-4f85-9fe5-0f710f95bc26"

    # if the script don't need output.
    try:
        auth_signature_calculation = subprocess.Popen(
            "php "
            + settings.STATIC_ROOT + "test.php "
            + oauth_token
            + " "
            + oauth_token_secret
            + " "
            + oauth_verifier,
            shell=True,
            stdout=subprocess.PIPE
        )
        auth_signature_calculation = auth_signature_calculation.stdout.read()
        auth_signature_calculation = str(auth_signature_calculation, 'utf-8')
        list = auth_signature_calculation.split("-")

        try:
            authorisation_value = "OAuth"
            authorisation_value += " oauth_nonce=\"" + list[0] + "\""
            authorisation_value += ", oauth_signature=\"" + list[1] + "\""
            authorisation_value += ", oauth_consumer_key=\"" + oauth_consumer_key + "\""
            authorisation_value += ", oauth_token=\"" + oauth_token + "\""
            authorisation_value += ", oauth_timestamp=\"" + list[2] + "\""
            authorisation_value += ", oauth_verifier=\"" + oauth_verifier + "\""
            authorisation_value += ", oauth_signature_method=\"HMAC-SHA1\""
            authorisation_value += ", oauth_version=\"1.0\""

            headers = {
                'Authorization': authorisation_value,
            }

            fetch_request_data = '{}'
            response = requests.post(
                'https://connectapi.garmin.com/oauth-service/oauth/access_token',
                headers=headers)

            fetch_request_data = __fetch_request_data(str(response.content, 'utf-8'))

            # save data
            if fetch_request_data["oauth_token_secret"]:
                garmin_api_data_utils_obj = GarminApiDataUtils()
                garmin_api_data_utils_obj.save_garmin_api_token_data_utils(email, fetch_request_data)
        except Exception as e:
            print(str(e))
    except Exception as e:
        print(str(e))
    return redirect(settings.FRONTEND_API + lang + "/profile")