from rest_framework.response import Response
from rest_framework.viewsets import ViewSet
from rest_framework import status
from apps.garmin_api.api.utils import GarminApiUtils
from rest_framework.permissions import IsAuthenticated
import boto3
from django.conf import settings

# garmin settings.
from requests_oauthlib import OAuth1Session


class GarminApiCreateRead(ViewSet):
    data_class = GarminApiUtils()
    permission_classes = (IsAuthenticated,)

    def specific_token(self, request, pk):
        response = {"success": False,
                    "message": ' Sorry no Garmin token exists for id :{}'.format(pk)}
        status_code = status.HTTP_400_BAD_REQUEST
        garmin_api_token_obj = self.data_class.get_garmin_api_token_utils(pk)
        if garmin_api_token_obj:
            response.update({'success': True})
            response.update({'message': 'Garmin user token retrieved successfully'})
            response.update({'data': garmin_api_token_obj})
            status_code = status.HTTP_200_OK
        return Response(response, status=status_code)

    def save_token(self, request, pk):
        response = {"success": False,
                    "message": '!!! Ops something went wrong '}
        garmin_token = request.POST.get('garmin_token')
        status_code = status.HTTP_400_BAD_REQUEST
        garmin_api_token_obj = self.data_class.save_garmin_api_token_utils(pk, garmin_token)
        if garmin_api_token_obj:
            response.update({'success': True})
            response.update({'message': 'Garmin user token saved successfully'})
            status_code = status.HTTP_200_OK
        return Response(response, status=status_code)

    def save_response_in_s3_bucket(self, request, pk):
        response = {"success": False,
                    "message": ' Sorry no requested s3 bucket exists.'}
        status_code = status.HTTP_400_BAD_REQUEST
        s3_bucket_name = str(pk)
        if s3_bucket_name:

            file = open("testfile.txt", "w")
            file.write("Hello World")
            file.close()

            # save in s3 bucket
            s3 = boto3.resource('s3')
            #  get the data and save.
            data = open(settings.STATIC_ROOT + 'just.json', 'rb')
            s3.Bucket(s3_bucket_name).put_object(Key='just.json', Body=data)
            response.update({'success': True})
            response.update({'message': 'The response saved in the s3 bucket {}.'.format(pk)})
            status_code = status.HTTP_200_OK
            with open(pk, 'w+') as fd:
                fd.write(str(response))
        return Response(response, status=status_code)

    def generate_garmin_link(self, request):

        try:
            client_key = "9e5a555d-42fd-4f85-9fe5-0f710f95bc26"
            client_secret = "tkhwOKfBR2cAaNxaW4NsHqb5qx01OnrSPTb"
            request_token_url = 'https://connectapi.garmin.com/oauth-service/oauth/request_token'
            oauth = OAuth1Session(client_key, client_secret=client_secret)
            fetch_response = oauth.fetch_request_token(request_token_url)
            oauth_token = fetch_response["oauth_token"]
            oauth_token_secret = fetch_response["oauth_token_secret"]
            make_link = "https://connect.garmin.com/oauthConfirm?oauth_token="
            make_link += oauth_token
            status_code = status.HTTP_200_OK
            response = {"success": True, "link": make_link, "token": oauth_token_secret}
            return Response(response, status=status_code)
        except Exception as e:
            response = {"success": False, "errors": str(e)}
            return Response(response, status=status_code)

    def callback_request_token(self, request):
        response = {"success": False, "errors": "ok"}
        return Response(response, status=200)
