from apps.garmin_api.api.data_utils import GarminApiDataUtils


class GarminApiUtils(object):
    data_class = GarminApiDataUtils()

    def get_garmin_api_token_utils(self, pk):
        return self.data_class.get_garmin_api_token_data_utils(pk)

    def save_garmin_api_token_utils(self, pk, data):
        return self.data_class.save_garmin_api_token_data_utils(pk, data)
