from django.conf.urls import url
from apps.garmin_api.api import views

urlpatterns = [
    url(r'^token/(?P<pk>[0-9a-z_@.]+)/$', views.GarminApiCreateRead.as_view({"get": "specific_token"}), name='specific_token'),
    url(r'^token/save/(?P<pk>[0-9a-z_@.]+)/$', views.GarminApiCreateRead.as_view({"put": "save_token"}), name='save_token'),
    url(r'^save/json/(?P<pk>[0-9a-z_-]+)/$', views.GarminApiCreateRead.as_view({"get": "save_response_in_s3_bucket"}),
        name='save_response_in_s3_bucket'),
    url(r'^generate/auth/link/$', views.GarminApiCreateRead.as_view({"get": "generate_garmin_link"}),
        name='generate_garmin_link')
]