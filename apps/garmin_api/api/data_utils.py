from apps.garmin_api.models import GarminToken
from simplecrypt import encrypt, decrypt
from base64 import b64encode, b64decode
from django.conf import settings


#https://blog.ruanbekker.com/blog/2018/04/29/encryption-and-decryption-with-simple-crypt-using-python/
class GarminApiDataUtils(object):

    def get_last_5000_records(self):
        try:
            user_data = GarminToken.objects.all().only("email").order_by("pk")[:5000]
        except GarminToken.DoesNotExist:
            user_data = None
        return user_data

    def get_garmin_api_token_detail(self, pk):
        try:
            token_detail = GarminToken.objects(email=str(pk)).get()
        except GarminToken.DoesNotExist:
            token_detail = None
        return token_detail

    # Get user garmin api token
    def get_garmin_api_token_data_utils(self, pk):

        token_detail = self.get_garmin_api_token_detail(pk)
        if token_detail:

            password = settings.SECRET_KEY
            try:

                # oauth_token
                oauth_token_b64decode = b64decode(token_detail.oauth_token)
                oauth_token = decrypt(settings.SECRET_KEY, oauth_token_b64decode)

                # oauth_token_secret
                oauth_token_secret_b64decode = b64decode(token_detail.oauth_token_secret)
                oauth_token_secret = decrypt(settings.SECRET_KEY, oauth_token_secret_b64decode)

                q_string_obj = {
                    "oauth_token": str(oauth_token, 'utf-8'),
                    "oauth_token_secret": str(oauth_token_secret, 'utf-8')
                }

            except Exception as e:
                q_string_obj = {"oauth_token": str(e), "oauth_token_secret": ""}

            return q_string_obj

    def save_garmin_api_token_data_utils(self, email, dict_data):

        if self and email:
            encoded_oauth_token_cipher = None
            encoded_oauth_token_secret_cipher = None
            password = settings.SECRET_KEY

            # settle oauth_token
            if dict_data["oauth_token"]:
                oauth_token_cipher = encrypt(password, dict_data["oauth_token"])
                encoded_oauth_token_cipher = b64encode(oauth_token_cipher)

            if dict_data["oauth_token_secret"]:
                oauth_token_secret_cipher = encrypt(password, dict_data["oauth_token_secret"])
                encoded_oauth_token_secret_cipher = b64encode(oauth_token_secret_cipher)

            if encoded_oauth_token_cipher and encoded_oauth_token_secret_cipher:
                # data data
                params = {}
                document = {}
                document['oauth_token'] = encoded_oauth_token_cipher
                document['oauth_token_secret'] = encoded_oauth_token_secret_cipher
                document['email'] = str(email)
                params['$set'] = document
                db = settings.PYMONGO_CONN
                client = db[str(settings.MONGO_SERVER['dbname'])]
                db_data = client['garmin_token']
                db_data.update_one({
                    'email': {'$eq': str(email)}
                },
                    params, upsert=True
                )
                db.close()
                return True
            else:
                return True