from django.apps import AppConfig


class GarminApiConfig(AppConfig):
    name = 'garmin_api'
