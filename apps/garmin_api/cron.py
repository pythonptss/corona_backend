from django_cron import CronJobBase, Schedule
from apps.garmin_api.api.data_utils import GarminApiDataUtils
import subprocess
import requests
import boto3
from django.conf import settings
import json
import uuid


# cron a) Will execute daily. fetch first 5000 email ids and keep in users.json file.
class CacheUsersEmails(CronJobBase):

    RUN_EVERY_MINS = 1
    schedule = Schedule(run_every_mins=RUN_EVERY_MINS)
    code = 'apps.garmin_api.cron.CacheUsersEmails'

    """
        - CRON Path 
        source /opt/venv/env/bin/activate && cd /var/www/sale_pts.loc/public_html/ && 
        python3 manage.py runcrons "apps.garmin_api.cron.CacheUsersEmails" --force      
    """
    def cache_users_email(self):

        try:
            dt_json = {}
            garmin_api_data_utils = GarminApiDataUtils()
            get_last_5000_records = garmin_api_data_utils.get_last_5000_records()
            for item in get_last_5000_records:
                print(item.email)
                user_chunk = {str(item.pk): item.email}
                dt_json.update(user_chunk)

            # write JSON File
            dest_file = settings.STATIC_ROOT + "users_cache" + "/" + 'users.json'
            with open(dest_file, 'w') as file:
                file.write(json.dumps(dt_json))
        except Exception as e:
            print(e)

    def do(self):
        self.cache_users_email()


class SendItemsToProcess(CronJobBase):

    RUN_EVERY_MINS = 1
    schedule = Schedule(run_every_mins=RUN_EVERY_MINS)
    code = 'apps.garmin_api.cron.SendItemsToProcess'

    """
        - CRON Path 
        source /opt/venv/env/bin/activate && cd /var/www/sale_pts.loc/public_html/ && 
        python3 manage.py runcrons "apps.garmin_api.cron.SendItemsToProcess" --force      
    """
    def callback_request_token(self, email, s3_bucket_name, link):

        garmin_api_data_utils = GarminApiDataUtils()
        token_detail = garmin_api_data_utils.get_garmin_api_token_data_utils(email)

        if token_detail["oauth_token_secret"]:
            oauth_token = token_detail["oauth_token"]
            oauth_token_secret = token_detail["oauth_token_secret"]
            oauth_consumer_key = "9e5a555d-42fd-4f85-9fe5-0f710f95bc26"

            # if the script don't need output.
            try:

                auth_signature_calculation = subprocess.Popen(
                    "php "
                    + settings.STATIC_ROOT
                    + "request_signing.php"
                    + " "
                    + oauth_token
                    + " "
                    + oauth_token_secret
                    + " "
                    + link,
                    shell=True,
                    stdout=subprocess.PIPE
                )

                auth_signature_calculation = auth_signature_calculation.stdout.read()
                auth_signature_calculation = str(auth_signature_calculation, 'utf-8')
                list = auth_signature_calculation.split("-")

                oauth_nonce = list[0]
                oauth_timestamp = list[2]
                upload_end_time_in_seconds = list[3]
                upload_start_time_in_seconds = list[4]

                try:
                    authorisation_value = "OAuth"
                    authorisation_value += " oauth_nonce=\"" + oauth_nonce + "\""
                    authorisation_value += ", oauth_signature=\"" + list[1] + "\""
                    authorisation_value += ", oauth_token=\"" + oauth_token + "\""
                    authorisation_value += ", oauth_consumer_key=\"" + oauth_consumer_key + "\""
                    authorisation_value += ", oauth_timestamp=\"" + oauth_timestamp + "\""
                    authorisation_value += ", oauth_signature_method=\"HMAC-SHA1\""
                    authorisation_value += ", oauth_version=\"1.0\""
                    headers = {
                        'Authorization': authorisation_value,
                    }

                    request_base_url = link + '?'
                    request_base_url += "uploadStartTimeInSeconds=" + upload_end_time_in_seconds
                    request_base_url += "&uploadEndTimeInSeconds=" + upload_start_time_in_seconds

                    fetch_request_data = '{}'
                    response = requests.get(request_base_url, headers=headers)
                    data = str(response.content, 'utf-8')

                    # unique name
                    unique_id = uuid.uuid1()
                    unique_name = str(unique_id) + "-unique.json"
                    print(unique_name)
                    if data:
                        s3 = boto3.resource('s3')
                        s3.Bucket(s3_bucket_name).put_object(Key= unique_name, Body= data)

                except Exception as e:
                    print(str(e))
            except Exception as e:
                print(str(e))

    def do(self):

        # write JSON File
        updated_json = {}
        destination_file = settings.STATIC_ROOT + 'users_cache/' + 'users.json'
        with open(destination_file, 'r') as file:
            file_data = json.load(file)
            if file_data:
                first_data_chunk = next(iter(file_data))
                garmin_user_email = file_data[first_data_chunk]
                del file_data[first_data_chunk]
                updated_json = file_data

        # update JSON File
        print(updated_json)
        dest_file = settings.STATIC_ROOT + 'users_cache/' + 'users.json'
        with open(destination_file, 'w') as users_data:
            users_data.write(json.dumps(updated_json))

        # push data to all links
        dict_links = {
            "bkt-pulse-ox": "https://healthapi.garmin.com/wellness-api/rest/pulseOx",
            "bkt-respiration": "https://healthapi.garmin.com/wellness-api/rest/respiration",
            "bkt-activities": "https://healthapi.garmin.com/wellness-api/rest/activities",
            "bkt-move-iq": "https://healthapi.garmin.com/wellness-api/rest/moveiq",
            "bkt-manually-updated-activities": "https://healthapi.garmin.com/wellness-api/rest/manuallyUpdatedActivities",
            "bkt-epochs": "https://healthapi.garmin.com/wellness-api/rest/epochs",
            "bkt-dailies": "https://healthapi.garmin.com/wellness-api/rest/dailies",
            "bkt-third-party-dailies": "https://healthapi.garmin.com/wellness-api/rest/thirdPartyDailies",
            "bkt-sleeps": "https://healthapi.garmin.com/wellness-api/rest/sleeps",
            "bkt-body-compositions": "https://healthapi.garmin.com/wellness-api/rest/bodyComps",
            "bkt-stress": "https://healthapi.garmin.com/wellness-api/rest/stressDetails",
            "bkt-user-metrics": "https://healthapi.garmin.com/wellness-api/rest/userMetrics"
        }

        for bkt_name, link in dict_links.items():
            self.callback_request_token(garmin_user_email, bkt_name, link)


class ProcessGarminUserDetail(CronJobBase):

    RUN_EVERY_MINS = 1
    schedule = Schedule(run_every_mins=RUN_EVERY_MINS)
    code = 'apps.garmin_api.cron.ProcessGarminUserDetail'

    """
        - CRON Path 
        source /opt/venv/env/bin/activate && cd /var/www/sale_pts.loc/public_html/ && 
        python3 manage.py runcrons "apps.garmin_api.cron.ProcessGarminUserDetail" --force       
    """

    def callback_request_token(self, email):

        garmin_api_data_utils = GarminApiDataUtils()
        token_detail = garmin_api_data_utils.get_garmin_api_token_data_utils(email)

        if token_detail["oauth_token_secret"]:
            oauth_token = token_detail["oauth_token"]
            oauth_token_secret = token_detail["oauth_token_secret"]
            oauth_consumer_key = "9e5a555d-42fd-4f85-9fe5-0f710f95bc26"

            # if the script don't need output.
            try:

                auth_signature_calculation = subprocess.Popen(
                    "php "
                    + settings.STATIC_ROOT
                    + "request_signing.php"
                    + " "
                    + oauth_token
                    + " "
                    + oauth_token_secret,
                    shell=True,
                    stdout=subprocess.PIPE
                )

                auth_signature_calculation = auth_signature_calculation.stdout.read()
                auth_signature_calculation = str(auth_signature_calculation, 'utf-8')
                list = auth_signature_calculation.split("-")

                oauth_nonce = list[0]
                oauth_timestamp = list[2]
                upload_end_time_in_seconds = list[3]
                upload_start_time_in_seconds = list[4]

                try:
                    authorisation_value = "OAuth"
                    authorisation_value += " oauth_nonce=\"" + oauth_nonce + "\""
                    authorisation_value += ", oauth_signature=\"" + list[1] + "\""
                    authorisation_value += ", oauth_token=\"" + oauth_token + "\""
                    authorisation_value += ", oauth_consumer_key=\"" + oauth_consumer_key + "\""
                    authorisation_value += ", oauth_timestamp=\"" + oauth_timestamp + "\""
                    authorisation_value += ", oauth_signature_method=\"HMAC-SHA1\""
                    authorisation_value += ", oauth_version=\"1.0\""
                    headers = {
                        'Authorization': authorisation_value,
                    }

                    request_base_url = 'https://healthapi.garmin.com/wellness-api/rest/activities?'
                    request_base_url += "uploadStartTimeInSeconds=" + upload_end_time_in_seconds
                    request_base_url += "&uploadEndTimeInSeconds=" + upload_start_time_in_seconds

                    fetch_request_data = '{}'
                    response = requests.get(request_base_url, headers=headers)
                    data = str(response.content, 'utf-8')
                    s3_bucket_name = "bkt-activities"

                    if data:
                        s3 = boto3.resource('s3')
                        s3.Bucket(s3_bucket_name).put_object(Key='justing.json', Body=data)
                    else:
                        print("its not empty")

                except Exception as e:
                    print(str(e))
            except Exception as e:
                print(str(e))

    def do(self):
        email = "prasherhere@gmail.com"
        self.callback_request_token(email)