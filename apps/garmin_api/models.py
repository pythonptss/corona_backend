from mongoengine import Document, fields


class GarminToken(Document):
    email = fields.StringField(required=True, unique=True)
    oauth_token = fields.StringField()
    oauth_token_secret = fields.StringField()