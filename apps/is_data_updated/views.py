from apps.user_detail.models import UserCharacteristics
from rest_framework import status
from rest_framework.response import Response
from rest_framework.generics import RetrieveUpdateDestroyAPIView, ListCreateAPIView
from rest_framework.permissions import IsAuthenticated
from django.conf import settings
from bson import ObjectId
from datetime import datetime


class GetIsDataUpdated(ListCreateAPIView):
    permission_classes = (IsAuthenticated,)

    def get_user_characteristics_object_id_by_email(self, email):
        try:
            user_characteristics = UserCharacteristics.objects(email=str(email)).get()
            user_characteristics_obj_id = user_characteristics.id
        except:
            user_characteristics_obj_id = None
        return user_characteristics_obj_id

    def is_today_survey_added(self, user_characteristics_id):

        now = datetime.now()
        date_str = now.strftime('%m/%d/%Y')
        datetime_object = datetime.strptime(date_str, '%m/%d/%Y')
        doc_ = datetime.combine(
            datetime_object,
            datetime.min.time()
        )
        db = settings.PYMONGO_CONN
        client = db[str(settings.MONGO_SERVER['dbname'])]
        db_data = client['daily_survey_report']
        cursor = db_data.find_one(
            {'created': {'$eq': doc_},
             'user_characteristics_id': ObjectId(str(user_characteristics_id))
             }
        )
        db.close()

        if cursor:
            return True
        else:
            return False

    def get(self, request, *args, **kwargs):

        is_data_updated = {"is_user_characteristics_updated": 2, "is_today_survey_added": 2}

        email = kwargs.get('email', None)
        user_characteristics_obj_id = self.get_user_characteristics_object_id_by_email(email=email)
        if user_characteristics_obj_id:
            is_data_updated["is_user_characteristics_updated"] = 1
            is_today_survey_added = self.is_today_survey_added(user_characteristics_obj_id)
            if is_today_survey_added:
                is_data_updated["is_today_survey_added"] = 1

        return Response(is_data_updated, status=status.HTTP_200_OK)