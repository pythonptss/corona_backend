from django.apps import AppConfig


class IsDataUpdatedConfig(AppConfig):
    name = 'is_data_updated'
