from django.conf.urls import url
from rest_framework_mongoengine import routers as merouters

merouter = merouters.DefaultRouter()

from django.urls import include, path, re_path
from . import views

urlpatterns = [
    re_path(r'^v1/is_data_updated/(?P<email>[0-9a-z_@.]+)$',
        views.GetIsDataUpdated.as_view(),
        name='get_is_data_updated_detail'
        )
]

urlpatterns += merouter.urls