from rest_framework_mongoengine import serializers
from apps.user_detail.models import UserCharacteristics


class UserCharacteristicsSerializer(serializers.DocumentSerializer):
    class Meta:
        model = UserCharacteristics
        fields = '__all__'