from .pagination import CustomPagination
from .serializers import UserCharacteristicsSerializer
from rest_framework import status
from rest_framework.response import Response
from rest_framework.generics import ListCreateAPIView
from rest_framework.permissions import IsAuthenticated
from apps.user_detail.models import UserCharacteristics
from django.conf import settings
from validate_email import validate_email
import re


class GetDeleteUpdateUserCharacteristics(ListCreateAPIView):
    serializer_class = UserCharacteristicsSerializer
    permission_classes = (IsAuthenticated,)
    pagination_class = CustomPagination

    def get_query(self, email):
        user_characteristics = UserCharacteristics.objects(email=str(email))
        return user_characteristics

    # Get all feed
    def get(self, request, *args, **kwargs):
        email = kwargs.get('email', None)
        user_characteristics = self.get_query(email)
        paginate_queryset = self.paginate_queryset(user_characteristics)
        serializer = self.serializer_class(paginate_queryset, many=True)
        return self.get_paginated_response(serializer.data)


# get(all)/post archive feed.
class GetPostUserCharacteristics(ListCreateAPIView):

    serializer_class = UserCharacteristicsSerializer
    permission_classes = (IsAuthenticated,)
    pagination_class = CustomPagination

    def get_queryset(self):
        archive_feed = UserCharacteristics.objects.order_by('-created')
        return archive_feed

    def get_document_by_email(self, email):

        feed = {}

        db = settings.PYMONGO_CONN
        client = db[str(settings.MONGO_SERVER['dbname'])]
        db_data = client['user_characteristics']
        cursor = db_data.find_one({'email_id': str(email)})
        db.close()
        if cursor:
            # mix array
            user_characteristics = {
                "email": str(cursor["email"]),
                "age": cursor["age"],
                "sex": cursor["sex"],
                "medical_condition": cursor["medical_condition"],
                "medical_condition_reason": str(cursor["medical_condition_reason"]),
                "height": cursor["height"],
                "weight": cursor["weight"],
                "weight_units": str(cursor["weight_units"]),
                "height_units": str(cursor["height_units"]),
                "sick_in_the_last_6_months": cursor["budget"],
                "country": cursor["country"],
                "state": cursor["state"]
            }
        else:
            user_characteristics = {}

        return user_characteristics

    def is_the_document_exists(self, email):
        db = settings.PYMONGO_CONN
        client = db[str(settings.MONGO_SERVER['dbname'])]
        db_data = client['user_characteristics']
        cursor = db_data.find_one({'email': str(email)})
        db.close()

        if cursor:
            return True
        else:
            return False

    RE_D = re.compile('\d')

    def has_digits(self, string):
        res = self.RE_D.search(string)
        return res is not None

    def extract_nbr(self, input_str):

        if self.has_digits(str(input_str)):
            if input_str is None or input_str == '':
                return 0
            out_number = ''
            for ele in input_str:
                if ele.isdigit():
                    out_number += ele
            return int(out_number)
        else:
            return 0

    def make_document(self, request):

        document = {}
        skills_list = []
        is_valid = False
        user_characteristics_document = {}

        """
        Receiving data:
        -   email
        -   age
        -   sex
        -   medical_condition
        -   medical_condition_reason
        -   height
        -   weight
        -   weight_units 
        -   sick_in_the_last_6_months
        -   sick_in_the_last_6_months_detail
        -   country
        -   state
        """

        email = request.POST.get('email')
        is_email_valid = validate_email(str(email))
        if email and is_email_valid:
            document['email'] = email
            is_valid = True

        age = request.POST.get('age')
        if age:
            document['age'] = self.extract_nbr(age)

        sex = request.POST.get('sex')
        if age:
            document['sex'] = self.extract_nbr(sex)

        medical_condition = request.POST.get('medical_condition')
        if medical_condition:
            document['medical_condition'] = self.extract_nbr(medical_condition)

        medical_condition_reason = request.POST.get('medical_condition_reason')
        if medical_condition_reason:
            document['medical_condition_reason'] = str(medical_condition_reason)

        height = request.POST.get('height')
        if height:
            document['height'] = self.extract_nbr(height)

        height_units = request.POST.get('height_units')
        if height_units:
            document['height_units'] = str(height_units)

        weight = request.POST.get('weight')
        if weight:
            document['weight'] = self.extract_nbr(weight)

        weight_units = request.POST.get('weight_units')
        if weight_units:
            document['weight_units'] = str(weight_units)

        sick_in_the_last_6_months = request.POST.get('sick_in_the_last_6_months')
        if sick_in_the_last_6_months:
            document['sick_in_the_last_6_months'] = self.extract_nbr(sick_in_the_last_6_months)

        sick_in_the_last_6_months_detail = request.POST.get('sick_in_the_last_6_months_detail')
        if sick_in_the_last_6_months_detail:
            document['sick_in_the_last_6_months_detail'] = str(sick_in_the_last_6_months_detail)

        country = request.POST.get('country')
        if height_units:
            document['country'] = str(country)

        state = request.POST.get('state')
        if height_units:
            document['state'] = str(state)

        if is_valid:
            user_characteristics_document = document

        return {"document": user_characteristics_document, "email": email, "is_valid": is_valid}

    # Get all users
    def get(self, request, *args, **kwargs):
        user_characteristics = self.get_queryset()
        paginate_queryset = self.paginate_queryset(user_characteristics)
        serializer = self.serializer_class(paginate_queryset, many=True)
        return self.get_paginated_response(serializer.data)

    # Create a new user characteristics
    def post(self, request, *args, **kwargs):

        user_characteristics_document = self.make_document(request)
        is_valid = user_characteristics_document["is_valid"]
        if is_valid:
            is_document_exists = self.is_the_document_exists(user_characteristics_document["email"])
            if is_document_exists:
                params = {}
                params['$set'] = user_characteristics_document["document"]
                db = settings.PYMONGO_CONN
                client = db[str(settings.MONGO_SERVER['dbname'])]
                db_data = client['user_characteristics']
                db_data.update_one({
                    'email': str(user_characteristics_document["email"])
                },
                    params, upsert=False
                )
                db.close()
                return Response({"message": "Updation Successful"}, status = status.HTTP_201_CREATED)
            else:

                # insert
                params_to_save = user_characteristics_document["document"]

                # make entry in archive data
                db = settings.PYMONGO_CONN
                client = db[str(settings.MONGO_SERVER['dbname'])]
                db_data = client['user_characteristics']
                db_data.save(params_to_save)
                db.close()
                return Response({"message": "Creation Successful"}, status=status.HTTP_201_CREATED)

        else:
            return Response({"message": "Email is empty."}, status=status.HTTP_404_NOT_FOUND)