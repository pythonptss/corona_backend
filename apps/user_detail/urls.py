from django.conf.urls import url
from rest_framework_mongoengine import routers as merouters

merouter = merouters.DefaultRouter()

from django.urls import include, path, re_path
from . import views

urlpatterns = [
    re_path(r'^v1/user_characteristics/(?P<email>[0-9a-z_@.]+)$',
        views.GetDeleteUpdateUserCharacteristics.as_view(),
        name='get_delete_update_user_characteristics'
        ),
    path('v1/user_characteristics/',
        views.GetPostUserCharacteristics.as_view(),
        name='get_user_characteristics'
        )
]

urlpatterns += merouter.urls