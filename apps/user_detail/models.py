from mongoengine import Document, fields


class UserCharacteristics(Document):
	email = fields.StringField(required=True, unique=True)
	age = fields.IntField()
	sex = fields.IntField()
	garmin_token = fields.StringField()
	medical_condition = fields.IntField()
	medical_condition_reason = fields.StringField()
	height = fields.IntField()
	height_units = fields.StringField()
	weight = fields.IntField()
	weight_units = fields.StringField()
	sick_in_the_last_6_months = fields.BooleanField(default=False)
	sick_in_the_last_6_months_detail = fields.StringField()
	country = fields.StringField()
	state = fields.StringField()